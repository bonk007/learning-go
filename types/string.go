package main

import (
	"fmt"
	"strings"
)

func main() {

	palindromChecker("mamah")

}

func palindromChecker(words string) {

	ar := strings.Split(words, "")
	li := len(ar) - 1
	nSplit := len(ar) / 2

	if len(ar)%2 == 0 {
		nSplit = li / 2
	}

	ltr := make([]string, nSplit)
	rtl := make([]string, nSplit)

	for i := 0; i < nSplit; i++ {
		rIdx := li - i
		rtl[i] = ar[rIdx]
		ltr[i] = ar[i]
	}

	if strings.Join(ltr, "") == strings.Join(rtl, "") {
		fmt.Println("YES, IT IS A PALINDROM")
	} else {
		fmt.Println("NO, IT IS NOT A PALINDROM")
	}

}
