package main

import "fmt"

func main() {
	printPrimes(30)
}

func printPrimes(n int) {

	for i := 1; i <= n; i++ {

		dividable := false

		k := 1

		for dividable == false && k <= i {

			if k != 1 && k != i {
				dividable = i%k == 0
			}

			k++
		}

		if dividable == false {
			fmt.Println(i)
		}

	}

}
