package main

import "fmt"

func main() {
	fmt.Println(couple(5, 3))
}

func couple(n int, k int) int {

	var counter int = 0

	for a := 1; a <= n; a++ {

		for b := a + 1; b <= n; b++ {

			c := a + b

			if c%k == 0 {
				counter += 1
			}

		}

	}

	return counter

}
