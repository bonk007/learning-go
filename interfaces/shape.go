package main

import "math"

type Shape interface {
	area() float64
}

type Square struct {
	w, h float64
}

type Circle struct {
	d float64
}

func (s *Square) area() float64 {
	return s.w * s.h
}

func (c *Circle) area() float64 {
	r := c.d / 2
	return math.Pi * r * r
}

func geometry(s Shape) int32 {
	return int32(s.area())
}

func main() {

	s := Square{6, 7}
	c := Circle{14}

	println(geometry(&s))
	println(geometry(&c))

}
