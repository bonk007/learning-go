package main

import (
	"fmt"
	"strings"
)

func main() {

	countingValeys(12, "DDUUDDUDUUUD")

}

func countingValeys(n int32, s string) {

	var (
		high      int32 = 0
		counter   int32 = 0
		isOnValey bool  = false
	)

	path := strings.Split(s, "")

	for _, dir := range path {

		if dir == "D" {
			high -= 1
		} else {
			high += 1
		}

		if !isOnValey && high < 0 {
			counter += 1
			isOnValey = true
		} else if high >= 0 {
			isOnValey = false
		}

		//fmt.Println(high)
	}

	fmt.Println(counter)

}
