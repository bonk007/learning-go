package main

import (
	"fmt"
)

func main() {

	arr := []int32{1, -2, 1, 0, 1, 2, -3, 0}

	plusMinus(arr)

}

func plusMinus(arr []int32) {

	var (
		zero     float64 = 0
		negative float64 = 0
		positive float64 = 0
	)

	for _, value := range arr {
		if value < 0 {
			negative += 1
		} else if value > 0 {
			positive += 1
		} else {
			zero += 1
		}
	}

	result := []float64{
		positive / float64(len(arr)),
		negative / float64(len(arr)),
		zero / float64(len(arr)),
	}

	for _, value := range result {
		fmt.Printf("%.4f\n", value)
	}

}
