package main

func main() {

	i := new(int)
	setOne(i)
	println(*i) // without prefix * will print hex memory address of the value
}

func setOne(i *int) {
	*i = 1
}
