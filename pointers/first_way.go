package main

func main() {

	var i int = 6

	setZero(&i)

	println(i)

}

func setZero(i *int) {
	*i = 0
}
