package main

import "fmt"

func main() {
	var (
		x int = 2
		y int = 3
	)

	swap(&x, &y)

	fmt.Printf("x: %d, y: %d", x, y)
}

func swap(x *int, y *int) {

	temp := *x
	*x = *y
	*y = temp
}
