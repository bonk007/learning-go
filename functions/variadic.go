package main

import "fmt"

func main() {

	fmt.Println(variadic(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))

}

func variadic(args ...int32) int32 {
	var total int32 = 0

	for _, value := range args {
		total += value
	}

	return total
}
