package main

import "fmt"

func main() {

	intVariable, stringVariable := multi()

	fmt.Println(stringVariable)
	fmt.Println(intVariable)

}

func multi() (int32, string) {
	return 2, "we return multiple values"
}
