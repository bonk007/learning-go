package main

func main() {

	closure := func(a int32, b int32) int32 {
		return a + b
	}

	println(closure(6, 8) + getClosure()())

}

// function returns closure
func getClosure() func() int32 {
	return func() (ret int32) {
		return 22
	}
}
