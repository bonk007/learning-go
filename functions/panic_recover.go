package main

func main() {
	defer recovery()
	panic("at the disco")
}

func recovery() {
	str := recover()
	println(str)
}
