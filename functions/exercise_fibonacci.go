package main

func main() {

	println(f(6))

}

func f(n int) int32 {

	if n < 1 {
		return 0
	}

	if n == 1 {
		return 1
	}

	return f(n-1) + f(n-2)

}
