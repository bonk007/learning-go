package main

import "fmt"

func main() {
	fmt.Println(without_param())
	fmt.Println(withParam(5, 6))
}

func without_param() string {
	return "Hello World! This is a simple function without parameters"
}

func withParam(a int32, b int32) int32 {
	return a + b
}
