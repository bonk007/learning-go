package main

func main() {
	defer second()
	first()
}

func first() {
	println("1st")
}

func second() {
	println("2nd")
}
